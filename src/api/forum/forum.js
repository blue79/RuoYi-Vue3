import request from '@/utils/request'

// 查询文章列表
export function listForum(query) {
  return request({
    url: '/forum/forum/list',
    method: 'get',
    params: query
  })
}

// 查询文章详细
export function getForum(id) {
  return request({
    url: '/forum/forum/' + id,
    method: 'get'
  })
}

// 新增文章
export function addForum(data) {
  return request({
    url: '/forum/forum',
    method: 'post',
    data: data
  })
}

// 修改文章
export function updateForum(data) {
  return request({
    url: '/forum/forum',
    method: 'put',
    data: data
  })
}

// 删除文章
export function delForum(id) {
  return request({
    url: '/forum/forum/' + id,
    method: 'delete'
  })
}
